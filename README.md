# Fujitsu General Plasma Display RS-232C control Utility

Tested on:
* P50XCA11JH

Should work with:
* PDS4213J-H, PDS4213W-H, PDS4213E-H
* PDS4214W-S, PDS4214E-S
* PDS4208W-B, PDS4208E-B
* PDS4209U-B
* other plasma display units from Fujitsu General

Warning: I didn't thoroughly verify the behavior of this program.
This program can fry your display.

## Usage

```
 usage:   fjpdpctl [-p port] <command>
 
  command: power (on|off)
           reset
           remotectl (allow|deny)
           composite
           rgb [12]
           svideo
           component
           aspect (normal|auto|wide1|wide2|zoom1|zoom2)
           videomode (auto|ntsc3|pal|secam|ntsc4|mpal|npal)
           remote (allow|deny)
           osdc (on|off)
           volume n <n:00..40 in hex>
```
