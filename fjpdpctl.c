/* Fujitsu Plasma Display RS-232C remote controller utility 
   2014 H.Tomari. Public Domain. */
#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
static char *portname="/dev/ttyUSB0";
#define ACKBUF_SZ 64

static int setportattr(int fd);
static char* waitForPDPack(int fd);
static int sendCommandToPDP(int fd, const char *cmd);
static int checkResponse(const char *res);
static int genericNoResultCommand(int fd, const char *cmd);
static int PDP$connect(int fd);
static int PDP$terminate(int fd);
static int PDP$pson(int fd);
static int PDP$psoff(int fd);
static int PDP$reset(int fd);
static int PDP$composite(int fd);
static int PDP$rgb(int fd, int idx);
static int PDP$svideo(int fd);
static int PDP$component(int fd);
static int PDP$remotectl(int fd, int sw);
static int PDP$aspect(int fd, int mode);
static int PDP$videomode(int fd, int mode);
static int PDP$remote(int fd, int sw);
static int PDP$osdc(int fd, int sw);
static int PDP$led(int fd, int sw);
static int PDP$volume(int fd, unsigned volume);
static void usage(void);
static unsigned setportname(unsigned this_state, char *arg);
static unsigned setvolume(unsigned this_state, char *arg);
extern int main(int argc, char *argv[]);

/* set port attr to 4800 8-n-1, RTS/CTS */
static int setportattr(int fd) {
	struct termios tm;
	if(tcgetattr(fd,&tm)!=0) {
		perror("tcgetattr");
		return -1;
	}
	tm.c_iflag &= ~(BRKINT | IGNPAR | PARMRK | INPCK | ISTRIP | INLCR | ICRNL | IXON | IXOFF);
	tm.c_oflag &= ~OPOST;
	tm.c_cflag &= ~(CSIZE | PARENB);
	tm.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
	tm.c_cflag |= (CS8 | CRTSCTS);
	cfsetispeed(&tm,B4800);
	cfsetospeed(&tm,B4800);
	if(tcsetattr(fd,TCSANOW,&tm)!=0) {
		perror("tcsetattr");
		return -2;
	}
	return 0;
}
/* Wait for response from PDP (max 4 seconds) */
static char* waitForPDPack(int fd) {
	static char ackbuf[ACKBUF_SZ];
	int wait_seconds=10;
	long ackbuf_off=0l;
	ssize_t nbytes;
	do {
		nbytes=read(fd,&ackbuf[ackbuf_off],ACKBUF_SZ-ackbuf_off);
		if((nbytes<0) && (errno!=EAGAIN) && (errno!=EWOULDBLOCK)) {
			perror("read");
			return NULL;
		} else if((nbytes>0) && (ackbuf[ackbuf_off+nbytes-1]==0x0d)) {
			return ackbuf;
		}
		if(nbytes>0) { ackbuf_off+=nbytes; }
		wait_seconds--;
		sleep(1);
	} while(wait_seconds>0);
	fputs("Request timed out.\n",stderr);
	return NULL;
}
/* send one line of command to PDP */
static int sendCommandToPDP(int fd, const char *cmd) {
	size_t len=strlen(cmd);
	size_t pos=0;
	do {
		ssize_t nbytes=write(fd,&cmd[pos],len-pos);
		if((nbytes<0) && (errno!=EAGAIN) && (errno!=EWOULDBLOCK)) {
			perror("write");
			return -1;
		}
		pos+=nbytes;
	} while(pos<len);
	return 0;
}

static int checkResponse(const char *res) {
	/*long i;
	fputs("response=>",stderr);
	for(i=0; i<15; i++) {
		fprintf(stderr,"%02X ",res[i]);
		if(res[i]=='\015') { break; }
	}*/
	return strncmp("@S\015",res,3);
}

/* Plasma Display command sender functions */
static int genericNoResultCommand(int fd, const char *cmd) {
	char *response;
	if(sendCommandToPDP(fd,cmd)<0) {
		fputs("sendCommandToPDP failed\n",stderr);
		return -1;
	}
	if((response=waitForPDPack(fd))==NULL) {
		fputs("no ack from PDP\n",stderr);
		return -1;
	}
	return checkResponse(response);
}
static int PDP$connect(int fd) {
	static const char conn_command[]="@G\015";
	return genericNoResultCommand(fd,conn_command);
}
static int PDP$terminate(int fd) {
	static const char term_command[]="@Q\015";
	return genericNoResultCommand(fd,term_command);
}
static int PDP$pson(int fd) {
	int res=genericNoResultCommand(fd,"!CRPN\015");
	sleep(7);
	return res;
}
static int PDP$psoff(int fd) {
	int res=genericNoResultCommand(fd,"!CRPF\015");
	sleep(5);
	return res;
}
static int PDP$reset(int fd) {
	return genericNoResultCommand(fd,"!CRST\015");
}
static int PDP$composite(int fd) {
	int res=genericNoResultCommand(fd,"!CRVO\015");
	sleep(3);
	return res;
}
static int PDP$rgb(int fd, int idx) {
	static const char rgb1_command[]="!CRRO\015";
	static const char rgb2_command[]="!CR2O\015";
	int res=genericNoResultCommand(fd,idx?rgb2_command:rgb1_command);
	sleep(3);
	return res;
}
static int PDP$svideo(int fd) {
	int res=genericNoResultCommand(fd,"!CRSO\015");
	sleep(3);
	return res;
}
static int PDP$component(int fd) {
	int res=genericNoResultCommand(fd,"!CRCO\015");
	sleep(3);
	return res;
}
static int PDP$remotectl(int fd, int sw) {
	static const char* rcctl_cmd[]={"!CSETR\015","!CSDTR\015"};
	return genericNoResultCommand(fd,rcctl_cmd[sw]);
}
static int PDP$aspect(int fd, int mode) {
	char basecmd[]="!CDI010X\015";
	const char modestr[]="012345";
	int res;
	basecmd[7]=modestr[mode];
	res=genericNoResultCommand(fd,basecmd);
	sleep((mode==1)?3:1);
	return res;
}
static int PDP$videomode(int fd, int mode) {
	char basecmd[]="!CDI030X\015";
	const char modestr[]="0123456";
	int res;
	basecmd[7]=modestr[mode];
	res=genericNoResultCommand(fd,basecmd);
	sleep(mode?1:3);
	return res;
}
static int PDP$remote(int fd, int sw) {
	static const char* rcctl_cmd[]={"!CSER\015","!CSDR\015"};
	return genericNoResultCommand(fd,rcctl_cmd[sw]);
}
static int PDP$osdc(int fd, int sw) {
	static const char* rcctl_cmd[]={"!COSDCON\015","!COSDCOFF\015"};
	return genericNoResultCommand(fd,rcctl_cmd[sw]);
}
static int PDP$led(int fd, int sw) {
	static const char* rcctl_cmd[]={"!CLED00\015","!CLED05\015"};
	return genericNoResultCommand(fd,rcctl_cmd[sw]);
}
static int PDP$volume(int fd, unsigned volume) {
	static char basecmd[]="CDI020XX\015";
	static const char hexdigits[]="0123456789ABCDEF";
	basecmd[6]=hexdigits[(volume>>4)];
	basecmd[7]=hexdigits[(volume&0xf)];
	return genericNoResultCommand(fd,basecmd);
}
/* Pervasive */
static void usage() {
	puts("fjpdpctl: Fujitsu General Plasma Display Control Utility\n"
	     "2014 H.Tomari. Public Domain.\n"
	     "\n"
	     "usage:   fjpdpctl [-p port] <command>\n"
	     "\n"
	     "  command: power (on|off)\n"
	     "           reset\n"
	     "           remotectl (allow|deny)\n"
	     "           composite\n"
	     "           rgb [12]\n"
	     "           svideo\n"
	     "           component\n"
	     "           aspect (normal|auto|wide1|wide2|zoom1|zoom2)\n"
	     "           videomode (auto|ntsc3|pal|secam|ntsc4|mpal|npal)\n"
	     "           remote (allow|deny)\n"
	     "           osdc (on|off)\n"
	     "           volume n <n:00..40 in hex>\n");
	exit(EXIT_SUCCESS);
}

typedef struct {
	unsigned base_state;
	char *token;
	unsigned next_state;
	unsigned (*callback)(unsigned this_state,char * arg);
} token_list_t;
static const token_list_t token_list[]={ /* state translation table */
	{0,	"-p",		1,	NULL},
	{0,	"power",	2,	NULL},
	{0,	"reset",	0x100,	NULL},
	{0,	"remotectl",	3,	NULL},
	{0,	"composite",	0x101,	NULL},
	{0,	"rgb",		0x102,	NULL},
	{0,	"svideo",	0x103,	NULL},
	{0,	"component",	0x104,	NULL},
	{0,	"aspect",	4,	NULL},
	{0,	"videomode",	5,	NULL},
	{0,	"remote",	6,	NULL},
	{0,	"osdc",		7,	NULL},
	{0,	"volume",	8,	NULL},
	{0,	"led",		10,	NULL},
	{0,	NULL,		9,	NULL},

	{1,	NULL,		0,	&setportname},

	{2,	"on",		0x105,	NULL},
	{2,	"off",		0x106,	NULL},
	{2,	NULL,		9,	NULL},

	{3,	"allow",	0x107,	NULL},
	{3,	"deny",		0x108,	NULL},
	{3,	NULL,		9,	NULL},

	{4,	"normal",	0x109,	NULL},
	{4,	"auto",		0x10a,	NULL},
	{4,	"wide1",	0x10b,	NULL},
	{4,	"wide2",	0x10c,	NULL},
	{4,	"zoom1",	0x10d,	NULL},
	{4,	"zoom2",	0x10e,	NULL},
	{4,	NULL,		9,	NULL},

	{5,	"auto",		0x115,	NULL},
	{5,	"ntsc3",	0x116,	NULL},
	{5,	"pal",		0x117,	NULL},
	{5,	"secam",	0x118,	NULL},
	{5,	"ntsc4",	0x119,	NULL},
	{5,	"mpal",		0x11a,	NULL},
	{5,	"npal",		0x11b,	NULL},
	{5,	NULL,		9,	NULL},

	{6,	"allow",	0x122,	NULL},
	{6,	"deny",		0x123,	NULL},
	{6,	NULL,		9,	NULL},

	{7,	"on",		0x124,	NULL},
	{7,	"off",		0x125,	NULL},
	{7,	NULL,		9,	NULL},

	{8,	NULL,		0,	&setvolume},

	{9,	NULL,		9,	NULL},

	{10,	"on",		0x126,	NULL},
	{10,	"off",		0x127,	NULL},
	{10,	NULL,		9,	NULL},

	{0x102,	"1",		0x102,	NULL},
	{0x102,	"2",		0x126,	NULL},
	{0x102,	NULL,		9,	NULL},

	{(unsigned)-1,	NULL,	9,	NULL}
};
static unsigned setportname(unsigned this_state, char *arg) {
	portname=arg;
	return 0;
}
static unsigned setvolume(unsigned this_state, char *arg) {
	unsigned long vol=strtol(arg,NULL,0x10);
	return 0x200|(vol&0xff);
}

extern int main(int argc, char *argv[]) {
	int port;
	int res=0;
	long argp=0;
	unsigned state=0;
	while(++argp<argc) {
		unsigned next_index=0;
		const token_list_t *tok;
		do {
			tok=&token_list[next_index++];
			if(state==tok->base_state) {
				if(!(tok->token) || 
				   !strcmp(tok->token,argv[argp])) {
					if(tok->callback) {
						state=(*tok->callback)(state,argv[argp]);
					} else {
						state=tok->next_state;
					}
					break;
				}
			}
		} while(tok->base_state!=(unsigned)-1);
	}
	if(state<0x100) { usage(); }
	if((port=open(portname,O_RDWR|O_NONBLOCK))<0) {
		perror("open");
		exit(1);
	}
	if(setportattr(port)<0) { res=1; goto fail; }
	if(PDP$connect(port)) {
		fputs("Connect command failed.\n",stderr);
		res=2;
		goto fail;
	}
	/* handle main command */
	if(state==0x100) {
		res=PDP$reset(port);
	} else if(state==0x101) {
		res=PDP$composite(port);
	} else if(state==0x102) {
		res=PDP$rgb(port,0);
	} else if(state==0x103) {
		res=PDP$svideo(port);
	} else if(state==0x104) {
		res=PDP$component(port);
	} else if(state==0x105) {
		res=PDP$pson(port);
	} else if(state==0x106) {
		res=PDP$psoff(port);
	} else if(state==0x107) {
		res=PDP$remotectl(port,0);
	} else if(state==0x108) {
		res=PDP$remotectl(port,1);
	} else if(0x109<=state && state<=0x10e) {
		res=PDP$aspect(port,state-0x109);
	} else if(0x115<=state && state<=0x11b) {
		res=PDP$videomode(port,state-0x115);
	} else if(state==0x122) {
		res=PDP$remote(port,0);
	} else if(state==0x123) {
		res=PDP$remote(port,1);
	} else if(state==0x124) {
		res=PDP$osdc(port,0);
	} else if(state==0x125) {
		res=PDP$osdc(port,1);
	} else if(state==0x126) {
		res=PDP$led(port,0);
	} else if(state==0x127) {
		res=PDP$led(port,1);
	} else if((state&0xf00)==0x200) {
		res=PDP$volume(port,state&0xff);
	} else {
		res=-1;
	}
	/* main command end */
	if(PDP$terminate(port)) {
		fputs("Terminate command failed.\n",stderr);
		if(res==0) res=3;
	}
fail:
	close(port);
	exit(res);
}
